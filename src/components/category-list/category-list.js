import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './category-list.css';

class Item extends Component {
	render() {
  	return(
      <li>
        <Link to = {this.props.link}> { this.props.name } </Link>
        { this.props.children }
      </li>
    );
  }
}

export default class CategoryList extends Component {
  
  onList (category) {

  	const children = (items) => {
    	if (items) {
      	return <ul>{ this.onList(items) }</ul>
      }
    }
    
    return category.map((node, index) => {
      return (
        <Item key = { index } name = { node.name } link = { node.link } id = { node.id }>
          { children(node.items) }
        </Item>
      );
    })

  };
  
  render() {
    const category = this.props.category;
  	return(
      <div className="app-category-list">
        <h1><span>Список категорий</span></h1>
        <ul>{ this.onList(category) }</ul>
      </div>
    );
  }
}
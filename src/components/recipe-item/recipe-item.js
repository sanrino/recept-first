import React from 'react';
import {Link} from 'react-router-dom';
import './recipe-item.css';

const RecipeItem = ({ recipes, itemId }) =>{
    const RecipeTemplate = recipes.map(function(item, index) {
        if(`${item.id}` === itemId){
            return (
                <div className="recipe" key={index}>
                    <div className="recipe-title"><h1><span>{item.name}</span></h1></div>
                    <div className="recipe-picture"><img src={item.imgLink} alt='some value'/></div>
                    <div className="recipe-caterory">
                        <Link to={`/${item.categoryMain}`}><span>{item.category}</span></Link>
                        <Link to={`/${item.categoryMain}/${item.categoryId}`}><span> • {item.childСategory}</span></Link>
                    </div>
                    <div className="recipe-description"><p></p><strong>ИНСТРУКЦИЯ ПРИГОТОВЛЕНИЯ:</strong> <p></p>{item.description}</div>

                </div>
            );
        }return null;
    });
    return (
        <div className="recipes">
            { RecipeTemplate }
        </div>
    );
};
export default RecipeItem;
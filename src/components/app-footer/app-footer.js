import React from 'react';
import './app-footer.css';

const AppFooter = () => {
  return (
    <div className="app-footer">
      <div className="container">
        <div className="copyright">© 2019 Recipes</div>
      </div>
    </div>
  );
};
export default AppFooter;